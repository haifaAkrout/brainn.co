# Serializers define the API representation.
from django.contrib.auth.models import User
from rest_framework import serializers, permissions

from ChallengeBrainnco.models import Profile


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username',
                  'email',
                  )


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    project_owner = serializers.PrimaryKeyRelatedField(many=False,
                                                       queryset=Profile.objects.all())
    user = serializers.StringRelatedField(many=False, read_only=True)
    class Meta:
        model = Profile
        fields = ('user',)




