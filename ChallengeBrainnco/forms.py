from django.forms import ModelForm, forms

from ChallengeBrainnco.models import User, Profile



class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email','password')

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('user',)



