from django.urls import re_path, path

from ChallengeBrainnco import views
from ChallengeBrainnco.api_urls import api_urlpatterns


urlpatterns = [

    re_path(r'^$', views.index, name='index'),
    re_path(r'editPage$', views.edit1, name='editPage'),
    re_path(r'^authentificate/$', views.authenticate_user, name='authentificate'),
    re_path(r'^logout/$', views.logout_view, name='logout'),
    re_path(r'^search/$', views.search, name='search'),
    path('repositories/', views.loader, name='list'),
    re_path(r'recommender$', views.recommender, name='recommender'),
    re_path(r'^profiles/create/$', views.add_profile, name='register'),
    re_path(r'^repositories/edit/$', views.edit_tag, name='edit'),



]
urlpatterns+=api_urlpatterns;