from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from ChallengeBrainnco import api_views

router = DefaultRouter()
router.register(r'api/usersSet', api_views.UserViewSet)

api_urlpatterns=[
    url(r'^api/users/$',api_views.users_list),

url(r'^', include(router.urls)),
]
api_urlpatterns += [
url(r'^api/api-auth/', include('rest_framework.urls',namespace='rest_framework'))
]
