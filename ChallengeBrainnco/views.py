import string
import threading

from git import *
import Algorithmia
import os

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
import requests
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import DetailView
import time
from ChallengeBrainnco.forms import ProfileForm, UserForm
from ChallengeBrainnco.models import User


def index(request):
    return render(request, 'index.html')


def edit1(request):
    list=[]
    reponame=request.GET['reponame']
    request.session['reponame']=reponame
    headers = {'Authorization': 'token ' + "a4c89728b8e5c6e8001a277929dad4b671bf1826"}
    response1 = requests.get('https://api.github.com/repos/'+request.session['username']+'/' + reponame + '/tags', headers=headers)
    geodata1 = response1.json();
    for i in geodata1:
        list.append(i['name'])
    seperator = ', '
    return render(request, 'editTag.html',{"tagnames":seperator.join(list)})


def loader(request):
    list = []
    request.session['username']=request.POST['username'];

    print(request.session['username']);
    headers = {'Authorization': 'token ' +"a4c89728b8e5c6e8001a277929dad4b671bf1826"}
    response = requests.get('https://api.github.com/users/'+request.session['username']+'/repos',headers=headers)
    geodata = response.json()
    for i in geodata:
        headers1 = {'Authorization': 'token ' + "a4c89728b8e5c6e8001a277929dad4b671bf1826"}
        response1 = requests.get('https://api.github.com/repos/'+request.session['username']+'/' + i['name'] + '/tags',headers=headers1)
        geodata1 = response1.json()
        i['tags']=geodata1;
        list.append(i);
        request.session['repositories']=list;
    return render(request, 'repositories.html', {
        'list': list,
    })

def add_profile(request):
    if request.method == "GET":
        form = ProfileForm()
        return render(request, 'profile.html', {'form': form})
    if request.method == "POST":
        form = ProfileForm(request.POST)
        if form.is_valid():
            postProfile = form.save(commit=False)
            postProfile.save()
            return HttpResponseRedirect(reverse('index'))
        else:
            return render(request, 'profile.html',
                          {'error_msg': 'error while creating the profile',
                           'form': form})


def authenticate_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            return HttpResponseRedirect(reverse('index'))
    else:
        return HttpResponseRedirect(reverse('index'))


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))



def edit_tag(request):
    tagnames=request.POST['tagnames'];
    reponame=request.session['reponame']
    headers = {'Authorization': 'token ' + "a4c89728b8e5c6e8001a277929dad4b671bf1826"}
    response1 = requests.get('https://api.github.com/repos/'+request.session['username']+'/' +reponame+ '/tags', headers=headers)
    geodata1 = response1.json();
    repo = Repo("/Users/atoufa traore/Documents/GitHub/"+reponame);
    for j in tagnames.split(','):
        for i in request.session['repositories']:
            if i['name'] == reponame:
                print(i['name'])
                if i['tags'] == []:
                    print("empty")
                    tag1 = repo.create_tag(j, message='Automatic tag"{0}"'.format(j));
                    repo.remotes.origin.push(tag1);
                else:
                    for m in i['tags']:
                        print(m['name']);
                        if m['name'] != j :
                            print(m['name']+"different")
                            tag1 = repo.create_tag(j, message='Automatic tag"{0}"'.format(j));
                            repo.remotes.origin.push(tag1);

                        else:
                            print("already exist");







    return HttpResponseRedirect(reverse('index'))


def search(request):
    list=[]
    tag=request.GET['search'];
    print(tag);
    for i in request.session['repositories']:
        for c in i['tags']:
            if c['name'] == tag:
                print("similar")
                list.append(i);
    return render(request, 'repositories.html', {
                'list': list,
            })
def recommender(request):

    return HttpResponseRedirect(reverse('index'))

