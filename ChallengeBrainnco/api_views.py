from django.contrib.auth.models import User
from django.http import JsonResponse
from rest_framework import status, generics, permissions, viewsets
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from ChallengeBrainnco.serializers import UserSerializer


@api_view(['GET', 'POST'])
def users_list(request):
    if request.method == 'GET':
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=status.HTTP_201_CREATED)

            return JsonResponse(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

class UserViewSet(viewsets.ModelViewSet):
            queryset = User.objects.all()
            serializer_class = UserSerializer


